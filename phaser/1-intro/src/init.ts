import * as Phaser from "phaser";
import { Example1 } from "./example1";
import { Example2 } from "./example2";
import { Example3 } from "./example3";

const config: Phaser.Types.Core.GameConfig = {
  type: Phaser.AUTO,
  width: 800,
  height: 600,
  physics: {
    default: "arcade",
    arcade: {
      gravity: { y: 200 }
    }
  },
  scene: [Example1, Example2, Example3]
};

const game = new Phaser.Game(config);
