export class Example2 extends Phaser.Scene {
  _text: Phaser.GameObjects.Text;
  constructor() {
    super({ key: "Example2" });
  }

  create() {
    this._text = this.add.text(0, 0, "Welcome Manos", { font: "40px Impact" });

    const tween = this.tweens.add({
      targets: this._text,
      x: 200,
      y: 250,
      duration: 2000,
      ease: "Elastic",
      easeParams: [1.5, 0.5],
      delay: 1000,
      onComplete: (src, tgt) => {
        tgt[0].x = 0;
        tgt[0].y = 0;
        tgt[0].setColor("Red");
      }
    });

    this.input.keyboard.on("keyup", e => {
      if (e.key == "1") {
        this.scene.start("Example1");
      }
    });
  }
}
