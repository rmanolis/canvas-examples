import * as Phaser from "phaser";

export class Example1 extends Phaser.Scene {
  _image?: Phaser.GameObjects.Image;
  _rec?: Phaser.GameObjects.Rectangle;
  _pixel?: Phaser.GameObjects.Rectangle;
  key_A: Phaser.Input.Keyboard.Key;
  constructor() {
    super({ key: "Example1" });
  }

  preload() {
    this.load.image("sprite", "assets/sprite.png");
  }

  create() {
    this._image = this.add.image(400, 300, "sprite");
    this._rec = this.add.rectangle(100, 100, 100, 100, 0x8aff33);
    this._pixel = this.add.rectangle(100, 200, 1, 1, 0x8aff33);


    this.input.keyboard.on('keyup_D', (event)=>{
      this._image.x += 10;
    });
    this.key_A = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A) 
    this.input.on('pointerdown',(event)=>{
      this._image.x = event.x
      this._image.y = event.y
    })

    this.input.keyboard.on('keyup_P', (event)=>{
      let physicsImage = this.physics.add.image(this._image.x, this._image.y,"sprite");
      physicsImage.setVelocity(Phaser.Math.RND.integerInRange(-100,100),-300)
    })

    this.input.keyboard.on('keyup',(e)=>{
      if(e.key == "2"){
        this.scene.start("Example2")
      }
      if(e.key == "3"){
        this.scene.start("Example3")
      }
    })
  }

  update(time: number, delta: number){
    if(this.key_A.isDown){
      this._image.x--;
    }
  }

}
