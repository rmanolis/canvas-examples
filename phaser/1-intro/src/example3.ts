import * as Phaser from "phaser";

export class Example3 extends Phaser.Scene{
    _soundPunch : Phaser.Sound.BaseSound;
    constructor(){
        super({key:"Example3"})
    }

    preload(){
        this.load.audio('punch',['assets/punch.mp3'])
    }

    create(){
        this._soundPunch = this.sound.add("punch",{loop:true})
        this._soundPunch.play()
    }
}