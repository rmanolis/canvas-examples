const canvas = document.querySelector("canvas");

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

const ctx = canvas.getContext("2d");

const mouse = {
  x: innerWidth / 2,
  y: innerHeight / 2
};

function createPixel() {
  let id = ctx.createImageData(1, 1); // only do this once per page
  id.data[0] = 0;
  id.data[1] = 0;
  id.data[2] = 0;
  id.data[3] = 255;
  return id;
}

addEventListener("click", event => {
  mouse.x = event.clientX;
  mouse.y = event.clientY;
  console.log(mouse);
});

function animate() {
  requestAnimationFrame(animate);
  //ctx.clearRect(0, 0, innerWidth, innerHeight);
  const id = createPixel();
  ctx.putImageData(id, mouse.x, mouse.y);
  //ctx.fillStyle = "black"
  //ctx.fillRect(mouse.x, mouse.y, 1, 1 )
}

animate();
