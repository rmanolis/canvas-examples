Lessons from 
https://www.youtube.com/playlist?list=PLpPnRKq7eNW3We9VdCfx9fprhqXHwTPXL



To know:
- if you type "html:5" + TAB, then it will create an HTML template
- "h1" + TAB
- The numbering (x,y) goes from left to right and down and start from the left corner
    for example (400,100) the point goes to right and up,
                (100,400) the point goes to the left and down
- to find the cooler color patterns
    https://color.adobe.com/explore
