
let canvas = document.querySelector('canvas');
let ctx = canvas.getContext('2d');

let img = new Image();

let input = document.getElementById('input');
input.addEventListener('change', handleFiles, false);
function handleFiles(e) {
    let url = URL.createObjectURL(e.target.files[0]);
    let img = new Image();
    img.onload = function() {
        ctx.drawImage(img, 0, 0);    
    }
    img.src = url;
    
}

let color = document.getElementById('color');
function pick(event) {
  let x = event.layerX;
  let y = event.layerY;
  let pixel = ctx.getImageData(x, y, 1, 1);
  let data = pixel.data;
  let rgba = 'rgba(' + data[0] + ', ' + data[1] +
             ', ' + data[2] + ', ' + (data[3] / 255) + ')';
  color.style.background =  rgba;
  color.textContent = rgba;
}
canvas.addEventListener('mousemove', pick);