const canvas = document.querySelector("canvas");
const c = canvas.getContext("2d");

canvas.width = innerWidth;
canvas.height = innerHeight;

const mouse = {
  x: innerWidth / 2,
  y: innerHeight / 2
};

const colors = ["#2185C5", "#7ECEFD", "#FFF6E5", "#FF7F66"];
const gravity = 1;
// Event Listeners
addEventListener("mousemove", event => {
  mouse.x = event.clientX;
  mouse.y = event.clientY;
});

addEventListener("resize", () => {
  canvas.width = innerWidth;
  canvas.height = innerHeight;

  init();
});

// Objects
class Ball {
  constructor(x, y, dy, radius, color) {
    this.x = x;
    this.y = y;
    this.dy = dy;
    this.radius = radius;
    this.color = color;
  }

  draw() {
    c.beginPath();
    c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
    c.fillStyle = this.color;
    c.fill();
    c.stroke()
    c.closePath();
  }

  update() {
    if (this.y + this.radius + this.dy > canvas.height) {
      this.dy = -this.dy * 0.90;
    } else {
      this.dy += gravity;
    }
    this.y += this.dy;
    this.draw();
  }
}


// Utils
function randomIntFromRange(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min)
}

// Implementation
let balls;
function init() {
  balls = [];
  const radius = 30;
  for (let i = 0; i < 200; i++) {
    let x = randomIntFromRange(0, canvas.width)
    let y = randomIntFromRange(0, canvas.height - radius)
    const ball = new Ball(x, y, 2, radius, "red");
    balls.push(ball);
  }
}

// Animation Loop
function animate() {
  requestAnimationFrame(animate);
  c.clearRect(0, 0, canvas.width, canvas.height);

  //c.fillText('HTML CANVAS BOILERPLATE', mouse.x, mouse.y)
  balls.forEach(ball => {
    ball.update();
  });
}

init();
animate();
