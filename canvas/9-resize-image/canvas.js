let canvas = document.querySelector("canvas");
canvas.width = innerWidth / 2;
canvas.height = innerHeight / 2;
let ctx = canvas.getContext("2d");

let input = document.getElementById("input");
input.addEventListener("change", handleFiles, false);
let img = new Image();

function handleFiles(e) {
  let url = URL.createObjectURL(e.target.files[0]);
  img.onload = function() {
    ctx.drawImage(img, 0, 0);
  };
  img.src = url;
}

function resize(){
  console.log("resize")
  ctx.clearRect(0, 0, innerWidth, innerHeight);

  ctx.drawImage(img, 0, 0, 1024, 576);
  // ctx.scale(1,0.5)
}

function rotate(){
  console.log("rotate")
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  //ctx.translate(canvas.width/2,canvas.height/2);
  ctx.rotate(Math.PI / 4);

  ctx.drawImage(img, 0, 0);
}
