const canvas = document.querySelector("canvas");

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

const c = canvas.getContext("2d");
const maxRadius = 60;
const colorArray = [
  "#ffaa33",
  "#99ffaaa",
  "#00ff00",
  "red",
  "pink",
  "blue",
  "grey",
  "brown"
];
let mouse = {
  x: undefined,
  y: undefined
};
window.addEventListener("mousemove", function(event) {
  mouse.x = event.x;
  mouse.y = event.y;
});

class Circle {
  constructor(x, y, dx, dy, radius, minRadius) {
    this.x = x;
    this.y = y;
    this.dx = dx;
    this.dy = dy;
    this.radius = radius;
    this.minRadius = minRadius
    this.color = colorArray[Math.floor(Math.random() * colorArray.length)];
  }

  draw() {
    c.beginPath();
    c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
    c.fillStyle = this.color;
    c.fill();
  }

  update() {
    if (this.x + this.radius > innerWidth || this.x - this.radius < 0) {
      this.dx = -this.dx;
    }

    if (this.y + this.radius > innerHeight || this.y - this.radius < 0) {
      this.dy = -this.dy;
    }
    this.x += this.dx;
    this.y += this.dy;
    if (
      mouse.x - this.x < 50 &&
      mouse.x - this.x > -50 &&
      mouse.y - this.y < 50 &&
      mouse.y - this.y > -50
    ) {
      if (this.radius < maxRadius) {
        this.radius += 1;
      }
    } else {
      if (this.radius > this.minRadius) {
        this.radius -= 1;
      }
    }
    this.draw();
  }
}

const circles = [];
for (let i = 0; i < 800; i++) {
  let radius = (Math.random() * 3  + 1) ;
  let x = Math.random() * (innerWidth - radius * 2) + radius;
  let y = Math.random() * (innerHeight - radius * 2) + radius;
  let dx = (Math.random() - 0.5) * 8; // velocity
  let dy = (Math.random() - 0.5) * 8; // velocity
  const circle = new Circle(x, y, dx, dy, radius, radius);
  circles.push(circle);
}
function animate() {
  requestAnimationFrame(animate);
  c.clearRect(0, 0, innerWidth, innerHeight);
  for (const circle of circles) {
    circle.update();
  }
}

animate();
