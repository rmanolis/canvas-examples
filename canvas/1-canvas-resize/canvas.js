const canvas = document.querySelector('canvas');

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;


const c = canvas.getContext('2d');
const x = 100;
const y = 100;
const width = 100;
const height = 100;
c.fillRect(x,y,width,height);

c.fillRect(200,200,width,height);